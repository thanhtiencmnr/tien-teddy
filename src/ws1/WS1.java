package ws1;

import java.util.Scanner;


public class WS1 {

   
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Enter number :");
        int n = s.nextInt();
        if (isPrime(n)) {
            System.out.println(n + " " + " is a prime number");
        } else {
            System.out.println(n + " " + " not prime");
        }

        System.out.println("prime numbers from 1 to 1000:");
        for (int i = 2; i < 1000; i++) {
            if (printPrimeList(i)) {
                System.out.print(i + " ");
            }
        }

        System.out.println(" ");

        System.out.println("1000 số nguyên tố đâu tiên: ");
        int num;
        //Scanner sc = new Scanner(System.in);
        num = 1000;//sc.nextInt();
        int i = 2;
        int count = 0;
        while (count <= num) {
            if (print1000FirstPrimes(i)) {
                System.out.print(i + " ");
                count++;
            }
            i++;
        }

        System.out.println(" ");

        solve();
        
    }

    

    public static void solve() {
        int a, b;
        double nghiem;
        Scanner s = new Scanner(System.in);
        System.out.println("Enter a:");
        a = s.nextInt();
        System.out.println("Enter b:");
        b = s.nextInt();
        System.out.println("New input method is: " + a + "x+ " + b + " =0. ");
        if (a == 0) {
            if (b == 0) {
                System.out.println("This equation has infinitely many solutions.");
            } else {
                System.out.println("The equation has no solution.");
            }
        } else {
            nghiem = (double) -b / a;
            System.out.println("Equation with solution x =" + nghiem + ".");
        }
    }

    public static boolean print1000FirstPrimes(int n) {
        if (n <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(n); i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static boolean printPrimeList(int n) {
        if (n <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(n); i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static boolean isPrime(int n) {
        if (n <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(n); i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
}    